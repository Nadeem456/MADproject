/* Goal:    Write code to connect to HTTP server(influxDB) and send data
        @Brief : 
        *The Hypertext Transfer Protocol (HTTP) is application-level protocol for collaborative,distributed,
        hypermedia information systems. 
        * It is the data communication protocol used to establish communication between client and server.
        *Below is the code to connect Http server and to send data.

        @author Prince Bhalawat
*/

/*########################################################################################*/

/** ---- Includes ---- */
/* ------------------- */

/** Standard includes*/
#include<stdio.h> // for Standard Input Output
#include<string.h> // for manipulating C strings and arrays
#include<stdlib.h> // for memory allocation, process control,conversions and others

/** Shunya interfaces header file*/
/** for sending JSON data through the http server*/
#include <si/shunyaInterfaces.h>

/* JSON parsing library */
#include "json/json.h" // to extract the data from JSON file in respective data type

/*########################################################################################*/

int main()
{
FILE *fp; // pointer variable to read file
char buffer[1024]; // buffer to store contents of JSON file
struct json_object *parsed_json; // structure to store entire JSON document
struct json_object *Vibration; // for storing Vibration data
struct json_object *val; // for storing each item of Vibration array
struct json_object *Sound; // for storing sound data
struct json_object *Temperature; // for storing Temperature data
struct json_object *Timestamp; // for storing timestamp
struct json_object *Battery; // for storing battery percentage


size_t n_Vibration; // varibale to store size of vibration array
size_t i; // counter varibale for FOR loop

fp = fopen("/etc/shunya/data.json","r"); // pointer to file location in local directory
fread(buffer, 1024, 1, fp);  // to read file contents and store it into buffer
fclose(fp); // closes file

parsed_json = json_tokener_parse(buffer); // to parse json file contents and convert it into json object


json_object_object_get_ex(parsed_json, "Vibration", &Vibration);
json_object_object_get_ex(parsed_json, "Sound", &Sound);
json_object_object_get_ex(parsed_json, "Temperature", &Temperature);
json_object_object_get_ex(parsed_json, "Timestamp", &Timestamp);
json_object_object_get_ex(parsed_json, "Battery", &Battery);

int sd = json_object_get_int(Sound);
int tmp = json_object_get_int(Temperature);
int ts = json_object_get_int(Timestamp);
int bt = json_object_get_int(Battery);


n_Vibration = json_object_array_length(Vibration);
int value;
int arr[3];
for(i=0;i<n_Vibration;i++) {
val = json_object_array_get_idx(Vibration, i);
i+1;
value = json_object_get_int(val);
arr[i] = value;}


influxdbObj testdb = newInfluxdb("test-db");//Creates a new Influxdb Instance and returns influxdbObj Instance for Influxdb

writeDbInflux (testdb,"motor_data","Vibration: %d \n %d \n %d \n  Sound: %d \n  Temperature: %d \n  Timestamp: %d  \n Battery: %d \n",arr[0],arr[1],arr[2],sd,tmp,ts,bt);//Send Data to Influx DB.

return 0;
}
